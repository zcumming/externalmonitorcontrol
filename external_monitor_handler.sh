#!/bin/bash

CONFIG=/Users/zcumming/.external_monitor_conf

function writeDefaultConfig()
{
	echo "MON_MUTE=true" > $CONFIG
	echo "MON_BRIGHT=0" >> $CONFIG
	echo "MON_VOL=10" >> $CONFIG
	chown zcumming:staff $CONFIG
}

function performDefaultConfig()
{
	toggleMute
	volCtrl 0
	brightCtrl 0
}

function readConfig()
{
	KEY=$1
	cat $CONFIG | grep $KEY | awk -F '=' '{print $2}'
}

function writeConfig()
{
	KEY=$1
	VALUE=$2
	CUR_VAL=$(readConfig $KEY)
	sed -i '' -e "s/$KEY=$CUR_VAL/$KEY=$VALUE/g" $CONFIG
}

function toggleMute()
{
	MUTE=$(readConfig MON_MUTE)
	if [ $MUTE = false ]; then
		/usr/local/bin/ddcctl -d 1 -m 1
		writeConfig MON_MUTE true
	else
		/usr/local/bin/ddcctl -d 1 -m 2
		writeConfig MON_MUTE false
	fi
}

function volCtrl()
{
	DELTA=$1
	CUR_VOL=$(readConfig MON_VOL)
	VOL=$(( $CUR_VOL + $DELTA ))
	if [ $VOL -le 0 ]; then
		VOL=0
	elif [ $VOL -ge 100 ]; then
		VOL=100
	fi
	/usr/local/bin/ddcctl -d 1 -v $VOL
	writeConfig MON_VOL $VOL	
}

function brightCtrl()
{
	DELTA=$1
	CUR_BRIGHT=$(readConfig MON_BRIGHT)
	BRIGHT=$(( $CUR_BRIGHT + $DELTA ))
	if [ $BRIGHT -le 0 ]; then
		BRIGHT=0
	elif [ $BRIGHT -ge 100 ]; then
		BRIGHT=100
	fi
	/usr/local/bin/ddcctl -d 1 -b $BRIGHT
	writeConfig MON_BRIGHT $BRIGHT	
}

function checkConfigExists()
{
if [ ! -f $CONFIG ]; then
	echo "Config file doesn't exist, use -d to create it"
	exit 1
fi
}

function printUsage()
{
	echo -e "External Monitor Handler - Controls external LG Monitor"
}

# Main
while getopts "mv:b:d" opt; do
	case $opt in
		m)
			checkConfigExists	
			toggleMute
			;;
		v)
			checkConfigExists
			volCtrl $OPTARG
			;;
		b)
			checkConfigExists
			brightCtrl $OPTARG
			;;
		d)
			writeDefaultConfig
			performDefaultConfig
			exit 0
			;;
		\?)
			echo "Invalid option: -$OPTARG"
			;;
	esac
done
