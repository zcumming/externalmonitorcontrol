# External Monitor Control #

Wraps ddcctl to provide keyboard shortcut-able CLI control of external monitor brightness, volume, and default values.
